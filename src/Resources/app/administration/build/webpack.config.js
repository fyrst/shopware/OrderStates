const { join, resolve } = require('path');
const path = require('path');

module.exports = () => { 
	return {
		resolve: { 
			alias: { 
				'fyrstOrderStates/component': resolve( 
						join(__dirname, '..', 'src', 'component') 
				),
			} 
		} 
	}; 
}
