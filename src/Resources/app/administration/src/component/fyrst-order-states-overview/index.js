import template from "./template.html.twig";
import './style.scss';


// eslint-disable-next-line no-undef
const { Component, Data, State, Context } = Shopware
const { Criteria } = Data

const { 
    mapState,
    mapMutations,
} = Component.getComponentHelper();

export default {
    template,

    inject: [
        'repositoryFactory',
    ],

    data () {
        return {
            defaultStateMachine: 'order.state',
            activeStateMachineStates: [
                { name: 'placeholder' }
            ],
            loadingStateMachineStates: true,
            gridColumns: [
                {
                    property: 'name',
                    label: this.$tc('fyrstOrderStates.labels.name'),
                    routerLink: 'fyrst.order.states.detail',
                    width: '30%'
                },
                {
                    property: 'technicalName',
                    label: this.$tc('fyrstOrderStates.labels.technicalName')
                }
            ],
            deleteItemId: null
        }
    },

    computed: {
        ...mapState('fyrstOrderStatesStore', [
            'stateMachines',
            'activeStateMachine',
        ]),

        stateMachineRepository () {
            return this.repositoryFactory.create('state_machine')
        },

        stateMachineStateRepository () {
            return this.repositoryFactory.create('state_machine_state')
        },

        stateMachineCriteria () {
            const criteria = new Criteria()
            return criteria
        },

        stateMachineStateCriteria () {
            const criteria = new Criteria()
            criteria.getAssociation('orders').setLimit(1)

            if (this.activeStateMachine) {
                criteria.addFilter(
                    Criteria.equals('stateMachineId', this.activeStateMachine.id)
                )
            }

            return criteria
        },
    },

    watch: {
        activeStateMachine () {
            this.fetchStateMachineStates()
        }
    },

    methods: {
        ...mapMutations('fyrstOrderStatesStore', [
            'setStateMachines',
            'setActiveStateMachine',
        ]),

        fetchStateMachines () {
            this.stateMachineRepository.search(
                this.stateMachineCriteria, 
                Context.api
            ).then((result) => {
                let defaultStateMachine = result.find((machine) => machine.technicalName === this.defaultStateMachine)
                
                if (defaultStateMachine === undefined) {
                    defaultStateMachine = result[0]
                }
                
                this.setStateMachines(result)
                
                if (this.activeStateMachine === null) {
                    this.setActiveStateMachine(defaultStateMachine)
                }
            })
        },

        fetchStateMachineStates () {
            this.loadingStateMachineStates = true

            this.stateMachineStateRepository.search(
                this.stateMachineStateCriteria, 
                Context.api
            ).then((result) => {
                this.activeStateMachineStates = result
                this.loadingStateMachineStates = false
            })
        },

        changeActiveStateMachine (value) {
            let stateMachine = this.stateMachines.find((machine) => machine.technicalName === value)

            if (stateMachine !== undefined && stateMachine.id !== this.activeStateMachine.id) {
                this.loadingStateMachineStates = true
                this.setActiveStateMachine(stateMachine)
            }
        },

        changeLanguage (value) {
            State.commit('context/setApiLanguageId', value)
            this.fetchStateMachines()
            this.fetchStateMachineStates()
        },

        deleteStateMachineState (id) {
            this.stateMachineStateRepository.delete(id, Context.api)
            .then((result) => {
                this.fetchStateMachineStates()
            }).catch((error) => {
                console.error(error)
            })

            this.resetDeleteStateMachineState()
        },

        prepareDeleteStateMachineState (value) {
            this.deleteItemId = value
        },

        resetDeleteStateMachineState () {
            this.deleteItemId = null
        }
    },

    created () {
        this.fetchStateMachines()
    },

    destroyed () {
        this.setActiveStateMachine(null)
    }
}