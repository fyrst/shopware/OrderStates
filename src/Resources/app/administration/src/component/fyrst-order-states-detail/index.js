import template from "./template.html.twig";
import './style.scss';

// eslint-disable-next-line no-undef
const { Component, Mixin, Data, State, Context } = Shopware
const { Criteria } = Data
const { 
    mapState,
    mapMutations
} = Component.getComponentHelper();

export default {
    template,

    inject: [
        'repositoryFactory'
    ],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    props: {
        stateMachineStateId: {
            type: String,
            required: false,
            default: null
        },

        isNew: {
            type: Boolean,
            required: false,
            default: false
        }
    },

    data () {
        return {
            isSaved: false,
            requiredFields: ['name', 'technicalName', 'stateMachineId']
        }
    },

    watch: {
        stateMachineStateId () {
            this.fetchStateMachineState()
        },

    },

    computed: {
        ...mapState('fyrstOrderStatesStore', [
            'activeStateMachineState',
        ]),

        stateMachineStateRepository () {
            return this.repositoryFactory.create('state_machine_state')
        },

        headerTitle () {
            if (this.isNew === true) {
                return this.$tc('fyrstOrderStates.action.newStateMachineState')
            }

            return this.activeStateMachineState?.translated?.name ?? null
        }
    },

    methods: {
        ...mapMutations('fyrstOrderStatesStore', [
            'setActiveStateMachineState',
        ]),

        fetchStateMachineState () {
            this.isSaved = false

            this.stateMachineStateRepository.get(
                this.stateMachineStateId, 
                Context.api
            ).then((result) => {
                this.setActiveStateMachineState(result)
            })
        },

        createStateMachineState () {
            State.commit('context/resetLanguageToDefault')
            this.setActiveStateMachineState(this.stateMachineStateRepository.create(Context.api))
        },

        changeLanguage (value) {
            State.commit('context/setApiLanguageId', value)
            this.fetchStateMachineState()
        },

        routePush (route, id) {
            console.log(route, id)
            this.$router.push({ name: route, params: { id } })
        },

        save () {
            let fieldErrors = []
            
            this.requiredFields.forEach(field => {
                if (this.activeStateMachineState[field] === undefined) {
                    fieldErrors.push({
                        field: field,
                        reason: 'empty'
                    })
                }
            });

            if (fieldErrors.length > 0) {
                this.createNotificationError({
                    message: this.$tc('fyrstOrderStates.messages.saveError')
                })

                return
            }

            this.stateMachineStateRepository.save(
                this.activeStateMachineState, 
                Context.api
            ).then((result) => {
                this.setActiveStateMachineState(null)
                this.createNotificationSuccess({
                    message: this.$tc('fyrstOrderStates.messages.saveSuccess')
                })
                this.isSaved = true

                if (this.isNew === true) {
                    this.routePush('fyrst.order.states.detail', JSON.parse(result.config.data).id)
                } else {
                    this.fetchStateMachineState()
                }
            }).catch((error) => {
                this.createNotificationError({
                    message: this.$tc('fyrstOrderStates.messages.saveError')
                })
            })
        }
    },

    created () {
        if (this.isNew === true) {
            this.createStateMachineState()
        } else {
            this.fetchStateMachineState()
        }
    }
}