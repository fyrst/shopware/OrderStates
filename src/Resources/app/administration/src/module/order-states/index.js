import store from './store';

// eslint-disable-next-line no-undef
const { Component, Module, State } = Shopware

State.registerModule('fyrstOrderStatesStore', store);

Component.register(
    'fyrst-order-states-overview', 
    () => import('fyrstOrderStates/component/fyrst-order-states-overview'))
Component.register(
    'fyrst-order-states-detail', 
    () => import('fyrstOrderStates/component/fyrst-order-states-detail')
)

Module.register('fyrst-order-states', {
    type: 'plugin',
    name: 'fyrstOrderStates',
    title: 'fyrstOrderStates.label',
    description: 'fyrstOrderStates.description',
    color: '#48D18A',
    icon: 'regular-sitemap',

    routes: {
        
        overview: {
            component: 'fyrst-order-states-overview',
            path: 'overview',
            meta: {
                icon: 'regular-sitemap',
                parentPath: 'sw.settings.index.plugins',
            }
        },

        detail: {
            component: 'fyrst-order-states-detail',
            path: 'detail/:id',
            props: {
                default (route) {
                    return {
                        stateMachineStateId: route.params.id
                    }
                }
            },
            meta: {
                icon: 'regular-sitemap',
                parentPath: 'fyrst.order.states.overview',
            }
        },

        create: {
            component: 'fyrst-order-states-detail',
            path: 'create',
            props: {
                default () {
                    return {
                        isNew: true
                    }
                }
            },
            meta: {
                parentPath: 'fyrst.order.states.overview'
            },
        }
    },

    settingsItem: [{
        group: 'plugins', // shop, system, plugins
        to: 'fyrst.order.states.overview',
        icon: 'regular-sitemap',
        label: 'fyrstOrderStates.label'
    }],

});