export default {
    namespaced: true,

    state() {
        return {
            // the state we want to keep track of
            stateMachines: null,
            activeStateMachine: null,
            activeStateMachineState: null,
        };
    },

    mutations: {
        // a mutation to change the state
        setStateMachines(state, stateMachines) {
            state.stateMachines = stateMachines;
        },
        setActiveStateMachine(state, activeStateMachine) {
            state.activeStateMachine = activeStateMachine;
        },
        setActiveStateMachineState(state, activeStateMachineState) {
            state.activeStateMachineState = activeStateMachineState;
        },
    }
};