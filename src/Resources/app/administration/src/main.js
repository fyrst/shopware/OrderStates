import deDE from './snippet/de-DE.json'
import enGB from './snippet/en-GB.json'
import "./module/order-states";

// eslint-disable-next-line no-undef
const { Locale } = Shopware

/** 
 * Extend text snippets
 */ 
Locale.extend('de-DE', deDE)
Locale.extend('en-GB', enGB)
